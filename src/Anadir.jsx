import React, {useState} from "react";
import {Redirect, Link} from "react-router-dom";
import {FormGroup, Label, Input} from 'reactstrap';
import { v4 as uuid } from "uuid";

export default (props) => {

  const [nom, setNom] = useState("");
  const [email, setEmail] = useState("");
  const [tel, setTel] = useState("");
  const [alta, setAlta] = useState("");
  const [volver, setVolver] = useState(false);

  const guardar = () => {
    props.addContact({
      id: uuid(),
      nom,
      email: email,
      tel: tel,
      alta: alta
    })
    setVolver(true);
  }

  if (volver){
    return <Redirect to="/contactos" />
  }

  return (
    <>
      <h3>{props.traduce('title').addContact}</h3>
      <hr />

      <FormGroup>
        <Label for="nom">{props.traduce('addForm')[0]}</Label>
        <Input type="text" name="nom" id="nom" value={nom} onChange={(e) => setNom(e.target.value)} />
      </FormGroup>

      <FormGroup>
        <Label for="email">{props.traduce('addForm')[1]}</Label>
        <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)}  />
      </FormGroup>
      
      <FormGroup>
        <Label for="tel">{props.traduce('addForm')[2]}</Label>
        <Input type="text" name="tel" id="tel" value={tel} onChange={(e) => setTel(e.target.value)}  />
      </FormGroup>

      <FormGroup>
        <Label for="tel">{props.traduce('addForm')[3]}</Label>
        <Input type="date" name="alta" id="alta" value={alta} onChange={(e) => setAlta(e.target.value)}  />
      </FormGroup>

      <hr />
      <Link className='btn btn-primary' to='/contactos' >{props.traduce('btnsAddForm')[0]}</Link>
      {' '}
      <button className='btn btn-success' onClick={guardar} >{props.traduce('btnsAddForm')[1]}</button>
    </>
  );
};

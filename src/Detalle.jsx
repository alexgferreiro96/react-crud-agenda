import React from "react";
import {Redirect, Link} from "react-router-dom";

export default (props) => {

  const id = props.match.params.id;
  const contacto = props.data.find((el) => el.id === id);

  if (!contacto){
    return <Redirect to="/contactos" />
  }

  return (
    <>
      <h3>Detalle</h3>
      <hr />
      <h1>Nombre: {contacto.nom}</h1>
      <h3>Email: {contacto.email}</h3>
      <h3>Tel: {contacto.tel}</h3>
      <hr />
      <Link className='btn btn-primary' to='/contactos' >Volver</Link>
    </>
  );
};

import React from "react";
import { Button, Table } from "reactstrap";
import {
  Link,
} from "react-router-dom";

export default (props) => {
  
  const filas = props.datos.map((el) => (
    <tr key={el.id}>
      {props.columnas.map((col, idx) => <td key={idx}>{col['nom'] === "alta" ? el[col['nom']].split("-").reverse().join("-") : el[col['nom']]}</td>)}
      <td><Link className="btn btn-success btn-sm" to={props.rutaShow + el.id} ><i className="fa fa-shower" aria-hidden="true"></i></Link></td>
      <td><Link className="btn btn-primary btn-sm" to={props.rutaEdit + el.id} ><i className="fa fa-pencil" aria-hidden="true"></i></Link></td>
      <td><Button className="btn btn-danger btn-sm" onClick={()=>props.deleteContact(el)} ><i className="fa fa-trash" aria-hidden="true"></i></Button> </td>
    </tr>
  ));

  return (
    <Table striped>
      <thead>
        <tr>
          {props.columnas.map((el, idx) => <th key={idx}>{el['titol']}</th>)}
          <th></th>
          <th></th>
        </tr>
      
      </thead>
      <tbody>{filas}</tbody>
      <tfoot></tfoot>
    </Table>
  );
};

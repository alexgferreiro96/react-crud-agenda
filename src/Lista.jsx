import React from "react";

import Taula from "./Taula";

export default (props) => {
  const columnes = [
    {
      nom: "nom",
      titol: props.traduce('tableList')[0],
    },
    {
      nom: "email",
      titol: props.traduce('tableList')[1],
    },
    {
      nom: "tel",
      titol: props.traduce('tableList')[2]
    },
    {
      nom: "alta",
      titol: props.traduce('tableList')[3]
    }
  ];

  return (
    <>
      <h3>{props.traduce('title').list}</h3>
      <Taula
        datos={props.data}
        columnas={columnes}
        rutaShow="/contactos/detalle/"
        rutaEdit="/contactos/editar/"
        deleteContact={props.deleteItemFromTaula}
      />
    </>
  );
};

const biblioteca = {
    title: [{home:"Mis contactos", list:"Listado", addContact:"Añadir contacto", edit: "Editar"}, {home:"Els meus contactes", list:"Llistat", addContact:"Afegir Contacte", edit: "Editar"}],
    nav: [["Home","Contactos","Añadir Contacto"], ["Home", "Contactes", "Afegir Contacte"]],
    tableList: [["Nombre", "Correo electronico", "Teléfono", "Fecha de alta"], ["Nom", "Correu electrónic", "Teléfon", "Data d'alta"]],
    addForm: [["Nombre", "Email", "Telefono", "Fecha de alta"], ["Nom", "Email", "Teléfon", "Data d'alta"]],
    btnsAddForm: [["Volver", "Añadir"], ["Tornar", "Afegir"]],
    btnsEditForm: [["Volver", "Aceptar Cambios"], ["Tornar", "Acceptar Canvis"]],
    modalMessage: ["Desea eliminar el siguiente contacto?", "Desitja eliminar el següent contacte?"]
}
export default biblioteca;
import React, {useEffect, useState} from "react";
import {
  BrowserRouter,
  NavLink,
  Switch,
  Route,
} from "react-router-dom";
import { Container } from "reactstrap";
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from "reactstrap";

import Home from "./Home";
import Detalle from "./Detalle";
import Edita from "./Editar";
import Lista from "./Lista";
import NotFound from "./P404";
import biblioteca from './biblioteca';

import "bootstrap/dist/css/bootstrap.min.css";
import Anadir from "./Anadir";

export default () => {

  const [idioma, setIdioma] = useState(0);

  const traduce = (etiqueta) => biblioteca[etiqueta][idioma]; 

  const [lista, setLista] = useState([]);
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const [userGonnaBeDelete, setUserGonnaBeDelete] = useState([]);

  useEffect(()=>{
    recuperar();
  },[]);

  useEffect(() => {
    localStorage.setItem('mis_contactos', JSON.stringify(lista));
  }, [lista]);

  

  const recuperar = () => {
    const itemsJson = localStorage.getItem('mis_contactos');
    const itemsLista = JSON.parse(itemsJson);
    if (itemsLista && itemsLista.length){
      setLista(itemsLista);
    } else {
      setLista([]);
    }

  }

  const addContact = (newContact) => {
    let newArrayAddContacts = [...lista, newContact];
    setLista(newArrayAddContacts);
  }

  const deleteItemLista = (contact) => {
    setUserGonnaBeDelete(contact);
    toggle();
  }

  const outDelete = () => {
    let newArrayDeleteContacts = lista.filter((item) => item.id !== userGonnaBeDelete.id);
    setLista(newArrayDeleteContacts);
    toggle();
  }

  return (
    <BrowserRouter>
    <Container fluid className="text-right p-3">
      <Button className="m-1" size='sm' color='primary' outline={idioma===0 ? false : true} onClick={() => setIdioma(0)}>ES</Button>
      <Button size='sm' color='primary' outline={idioma===1 ? false : true} onClick={() => setIdioma(1)}>CA</Button></Container>
      <Container>
        <br />
        <ul className="nav nav-tabs">
          <li className="nav-item">
            <NavLink exact className="nav-link" to="/">
              {traduce('nav')[0]}
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/contactos">
              {traduce('nav')[1]}
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/add">
              {traduce('nav')[2]}
            </NavLink>
          </li>
        </ul>
        <br />
        <br />
        <Switch>
          <Route exact path="/" render={()=><Home traduce={traduce} />} />
          <Route exact path="/contactos" render={() => <Lista deleteItemFromTaula={deleteItemLista} data={lista} traduce={traduce} />} />
          <Route exact path="/add" render={()=> <Anadir addContact={addContact} traduce={traduce} />} />
          <Route path="/contactos/detalle/:id" render={(props) => <Detalle {...props} data={lista} traduce={traduce} />} />
          <Route path="/contactos/editar/:id" render={(props) => <Edita {...props} data={lista} setLista={setLista} traduce={traduce} />} />
          <Route component={NotFound} />
        </Switch>
      </Container>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader>{traduce('modalMessage')}</ModalHeader>
        <ModalBody>
          {traduce('addForm')[0]}: {userGonnaBeDelete.nom} <br />
          {traduce('addForm')[1]}: {userGonnaBeDelete.email} <br />
          {traduce('addForm')[2]}: {userGonnaBeDelete.tel} <br />
          {traduce('addForm')[3]}: {userGonnaBeDelete.alta} <br />
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={outDelete}>Borrar</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </BrowserRouter>
  );
};

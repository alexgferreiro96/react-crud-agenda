import React, {useState} from "react";
import {Redirect, Link} from "react-router-dom";
import {FormGroup, Label, Input} from 'reactstrap';

export default (props) => {

  const id = props.match.params.id;
  const cliente = props.data.find((el) => el.id === id);

  if (!cliente){
    return <Redirect to="/contactos" />
  }

  const [nom, setNom] = useState(cliente.nom);
  const [email, setEmail] = useState(cliente.email);
  const [tel, setTel] = useState(cliente.tel);
  const [alta, setAlta] = useState(cliente.alta);
  const [volver, setVolver] = useState(false);

  const guardar = () => {
    const clienteModificado = {
      id: cliente.id,
      nom,
      email: email,
      tel: tel,
      alta: alta
    };

    const nuevaLista = props.data.map(el => {
      if(el.id === clienteModificado.id){
        return clienteModificado;
      }
      return el;
    })
    props.setLista(nuevaLista);
    setVolver(true);
  }

  if (volver){
    return <Redirect to="/contactos" />
  }

  return (
    <>
      <h3>{props.traduce('title').edit}</h3>
      <hr />

      <FormGroup>
        <Label for="nom">{props.traduce('addForm')[0]}</Label>
        <Input type="text" name="nom" id="nom" value={nom} onChange={(e) => setNom(e.target.value)} />
      </FormGroup>

      <FormGroup>
        <Label for="email">{props.traduce('addForm')[1]}</Label>
        <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)}  />
      </FormGroup>
      
      <FormGroup>
        <Label for="tel">{props.traduce('addForm')[2]}</Label>
        <Input type="text" name="tel" id="tel" value={tel} onChange={(e) => setTel(e.target.value)}  />
      </FormGroup>

      <FormGroup>
        <Label for="tel">{props.traduce('addForm')[3]}</Label>
        <Input type="date" name="alta" id="alta" value={alta} onChange={(e) => setAlta(e.target.value)}  />
      </FormGroup>

      <hr />
      <Link className='btn btn-primary' to='/contactos' >{props.traduce('btnsEditForm')[0]}</Link>
      {' '}
      <button className='btn btn-success' onClick={guardar} >{props.traduce('btnsEditForm')[1]}</button>
    </>
  );
};
